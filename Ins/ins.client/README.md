# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh



Nisam napravila cijeli projekt jer sam se dugo vremena mučila s povezivanjem frontenda i backenda. Na kraju ih nisam mogla odvojene spojiti pa sam u Visual Studiu 2022 kreirala projekt React and ASP.NET Core tako da su mi u jednom projektu i frontend i backend i tako sam ih uspjela povezati. Sve se pokreće tako da se cijeli folder Ins otvori u Visual Studiu 2022, zatim se kao glavni projekt odabere ins.client i u konzoli se pokrene npm install da se instaliraju sve potrebne stvari. (Jedna napomena, zbog toga što sam morala frontend ubacivati u ovaj projekt dosta se template-a što se tiče css-a nije učitalo pa je izgled drugačiji tj bazičan, a nisam ga stigla uređivat jer sam se više pozabavila s funkcionalnošću.) Zatim se stavi da su oba projekta jednake važnosti tj. da niti jedan nije glavni, odnosno poništi se ono što smo stavili da je ins.client glavni i pokrenemo u solutionu. U backendu sam napisala:
- modele za entitete student, professor, subject, instructionsDate
- view modele za registracije, login i kreiranje novog predmeta
- data za povezivanje s bazom i kreiranje svih tablica
- kontrolere za student, professor, subject, instructionsDate
- utils

Što se tiče povezivanja backenda i frontenda uspjela sam ostvariti da radi:
 - login
 - logout
 - kreiranje novog predmeta
 - da se na homepageu prikažu svi predmeti 
 - da se na homepageu prikažu svi profesori
 - kad se stisne na neki predmet da se ode na stranicu tog predmeta (jedino se informacije ne prikažu)
 - registracija radi preko postmana, ali ne preko stranice 

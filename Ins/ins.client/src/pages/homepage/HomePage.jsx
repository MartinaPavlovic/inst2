import "./HomePage.css";

import { Link } from "react-router-dom";
import { useState, useEffect } from 'react';

import InputAdornment from "@mui/material/InputAdornment";
import ProfessorsComponent from "../../components/professors/ProfessorsComponent";

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';


function ComboBox() {
  const [subjects, setSubjects] = useState([]);

    useEffect(() => {
        fetch("api/Subjects")
            .then(response => response.json())
            .then(data => {
                setSubjects(data);
                console.log(data); // Provjerite da li se podaci ispravno dohvaćaju
            })
            .catch(error => console.error('Error fetching students:', error)); // Uhvatite greške
    }, []);

  const handleSubjectSelect = (event, value) => {
    if (value) {
      window.location.href = `/subject/${value.url}`;
    }
  };

  return (
    <>
    <div className="search-container">
    <Autocomplete
        disablePortal
        id="combo-box-demo"
        options={subjects && subjects.subjects ? subjects.subjects : []}
        getOptionLabel={(option) => option.title} 
        onChange={handleSubjectSelect}
        renderInput={(params) => (
            <TextField
                {...params}
                InputProps={{
                ...params.InputProps,
                startAdornment: (
                <InputAdornment position="start">
                    
                </InputAdornment>
            ),
        }}
     />
  )}
 />
    {/*<Button variant="contained" onClick={handleButtonClick}>Pretraži</Button> */}
    </div>
    <div>
        {subjects && subjects.subjects && subjects.subjects.map(subject => (
        <Link
          to={`/subject/${subject.url}`}
          key={subject.url}
          className="link-no-style"
        >
          <div className="predmet">
            <h2 className="predmet-text">{subject.title}</h2>
            <p className="predmet-text">{subject.description}</p>
          </div>
        </Link>
      ))}
    </div>
    </>
  );
}

function HomePage() {
  const [professors, setProfessors] = useState([]);

     useEffect(() => {
        fetch("api/Professors")
        .then(response => response.json())
            .then(data => {
                setProfessors(data);
                console.log(data); // Provjerite da li se podaci ispravno dohvaćaju
             })
             .catch(error => console.error('Error fetching students:', error)); // Uhvatite greške
      }, []);

  if (!localStorage.getItem('token')) {
    window.location.href = '/login';
  }

  return (
    <>
      <div className="homepage-wrapper">
        <div className="homepage-container">
          <div>
            <div className="title">
            <img src="/logo/dotGet-logo.svg" alt="" />
            <h2>instrukcije po mjeri!</h2>
            <ComboBox />

          </div>

          <div>
            <h4>Najpopularniji instruktori:</h4>
            <ProfessorsComponent
              professors={professors}
              showSubject={true}
              showInstructionsCount={false}
            />
          </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default HomePage;

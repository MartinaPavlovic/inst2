import "./SubjectPage.css";
import ProfessorsComponent from "../../components/professors/ProfessorsComponent";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

async function getSubject(url) {
    try {
        const response = await fetch("api/subjects/" + url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + localStorage.getItem("token"),
            },
        });

        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        const subject = await response.json(); // Await response.json() to get the JSON data
        console.log("Odgovor s API-ja:", subject);
        return subject;
    } catch (error) {
        console.error("There has been a problem with your fetch operation:", error);
    }
}

function SubjectPage() {
    const { subjectName } = useParams();
    const [subjectData, setSubjectData] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const data = await getSubject(subjectName);
            console.log("Odgovor s API-ja:", data);
            setSubjectData(data);
            
        };

        fetchData();
    }, [subjectName]);

    return (
        <>
            <div className="subjectPage-wrapper">
                <div className="subjectPage-container">
                    <div>
                        <div className="subjectPage-title">
                            <h1>{subjectData ? subjectData.subject.title : "Ime predmeta"}</h1>
                            <p>{subjectData ? subjectData.subject.description : "Opis predmeta"}</p>
                        </div>
                    </div>

                    <div>
                        <h4>Najpopularniji instruktori:</h4>
                        <ProfessorsComponent
                            professors={subjectData ? subjectData.professors : []}
                            showSubject={false}
                            showInstructionsCount={true}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default SubjectPage;

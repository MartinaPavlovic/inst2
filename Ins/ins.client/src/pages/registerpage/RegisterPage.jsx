import { useState } from "react";
import {
  Button,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import "./RegisterPage.css";
import { useEffect } from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";


export const handlerRegister = async (formData, user) => {
    try {
        const response = await fetch("api/professorss/register", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
        });

        if (response.status === 201) {
            console.log(user + " registered successfully");
            window.location.href = "/login";
        } else {
            console.error(response.status + " " + response.statusText);
        }
    } catch (error) {
        console.error("An error occurred:", error);
    }
};

export const handlerRegisterStudent = async (formData, user) => {
    try {
        const response = await fetch("api/students/register", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
        });

        if (response.status === 201) {
            console.log(user + " registered successfully");
            window.location.href = "/login";
        } else {
            console.error(response.status + " " + response.statusText);
        }
    } catch (error) {
        console.error("An error occurred:", error);
    }
};

function RegisterPage() {
  const [showStudentForm, setShowStudentForm] = useState(true);

  const [studentName, setStudentName] = useState("");
  const [studentSurname, setStudentSurname] = useState("");
  const [studentEmail, setStudentEmail] = useState("");
  const [studentPassword, setStudentPassword] = useState("");
  const [studentConfirmPassword, setStudentConfirmPassword] = useState("");
    const [studentProfilePicture, setStudentProfilePicture] = useState("");
  const [errorMessage, setErrorMessage] = useState('');

  const [professorName, setProfessorName] = useState("");
  const [professorSurname, setProfessorSurname] = useState("");
  const [professorEmail, setProfessorEmail] = useState("");
  const [professorPassword, setProfessorPassword] = useState("");
  const [professorConfirmPassword, setProfessorConfirmPassword] = useState("");
  const [professorProfilePicture, setProfessorProfilePicture] = useState(null);
  const [professorSubjects, setProfessorSubjects] = useState([]);

  const handleStudentSubmit = async (event) => {
    event.preventDefault();

    if (studentPassword !== studentConfirmPassword) {
      setErrorMessage('Lozinke se ne podudaraju.');
      return;
    }

    // Prepare the data to be sent
    const studentData = new FormData();
      studentData.append("name", studentName);
      console.log("Ime studenta:", studentName);
      studentData.append("surname", studentSurname);
      console.log("prezime studenta:", studentSurname);
      studentData.append("email", studentEmail);
      console.log("mail studenta:", studentEmail);
      studentData.append("password", studentPassword);
      console.log("password studenta:", studentPassword);
      studentData.append("profilePicture", studentProfilePicture);
      console.log("slika studenta:", studentProfilePicture);

    console.log("Podaci studenta koji se šalju na backend:", studentData);

    console.log(studentData);

    // Send the data to the server
    handlerRegisterStudent(studentData, "student");
  };

  const handleProfessorSubmit = async (event) => {
    event.preventDefault();

    const professorData = new FormData();
    professorData.append("name", professorName);
    professorData.append("surname", professorSurname);
    professorData.append("email", professorEmail);
    professorData.append("password", professorPassword);
    professorData.append("profilePicture", professorProfilePicture);
    professorData.append("subjects", professorSubjects.map((s) => s.url));

    console.log(professorData);

    handlerRegister(professorData, "professor");
  };

  const handleStudentImageChange = (event) => {
    setStudentProfilePicture(event.target.files[0]);
  };

  const handleProfessorImageChange = (event) => {
    setProfessorProfilePicture(event.target.files[0]);
  };

  const [subjects, setSubjects] = useState([]);

    useEffect(() => {
        fetch("/Subjects")
            .then(response => response.json())
            .then(data => setSubjects(data))
    }, [])

  const handleSubjectSelect = (event, value) => {
    if (value) {
      setProfessorSubjects((prevSubjects) => [...prevSubjects, value]);
    }
  };


  return (
    <>
      {showStudentForm ? (
        <div className="register-wrapper">
          <div className="register-container">
            <h1>Registracija studenta</h1>
            <form onSubmit={handleStudentSubmit}>
              <div className="register-form">
                <InputLabel htmlFor="name">Ime</InputLabel>
                <OutlinedInput
                  id="name"
                  type="text"
                  value={studentName}
                  onChange={(e) => setStudentName(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/person-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="surname">Prezime</InputLabel>
                <OutlinedInput
                  id="surname"
                  type="text"
                  value={studentSurname}
                  onChange={(e) => setStudentSurname(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/person-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="email">Email</InputLabel>
                <OutlinedInput
                  id="email"
                  type="email"
                  value={studentEmail}
                  onChange={(e) => setStudentEmail(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/email-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="password">Lozinka</InputLabel>
                <OutlinedInput
                  id="password"
                  type="password"
                  value={studentPassword}
                  onChange={(e) => setStudentPassword(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/password-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="confirmPassword">
                  Potvrdi Lozinku
                </InputLabel>
                <OutlinedInput
                  id="confirmPassword"
                  type="password"
                  value={studentConfirmPassword}
                  onChange={(e) => setStudentConfirmPassword(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/password-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="profilePicture">
                  Profilna fotografija
                </InputLabel>
                <OutlinedInput
                  id="profilePicture"
                  type="file"
                  onChange={handleStudentImageChange}
                />
                {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}
              </div>
              <Button
                type="submit"
                variant="contained"
                style={{ marginRight: "1rem" }}
              >
                Registriraj se
              </Button>
              <Button
                type="button"
                variant="contained"
                onClick={() => {
                  setStudentName("");
                  setStudentSurname("");
                  setStudentEmail("");
                  setStudentPassword("");
                  setStudentConfirmPassword("");
                  setStudentProfilePicture(null);
                }}
              >
                Odbaci
              </Button>
            </form>
          </div>
        </div>
      ) : (
        <div className="login-wrapper">
          <div className="login-container">
            <h1>Registracija profesora</h1>
            <form onSubmit={handleProfessorSubmit}>
              <div className="register-form">
                <InputLabel htmlFor="name">Ime</InputLabel>
                <OutlinedInput
                  id="name"
                  type="text"
                  value={professorName}
                  onChange={(e) => setProfessorName(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/person-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="surname">Prezime</InputLabel>
                <OutlinedInput
                  id="surname"
                  type="text"
                  value={professorSurname}
                  onChange={(e) => setProfessorSurname(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/person-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="email">Email</InputLabel>
                <OutlinedInput
                  id="email"
                  type="email"
                  value={professorEmail}
                  onChange={(e) => setProfessorEmail(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/email-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="password">Lozinka</InputLabel>
                <OutlinedInput
                  id="password"
                  type="password"
                  value={professorPassword}
                  onChange={(e) => setProfessorPassword(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/password-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="confirmPassword">
                  Potvrdi Lozinku
                </InputLabel>
                <OutlinedInput
                  id="confirmPassword"
                  type="password"
                  value={professorConfirmPassword}
                  onChange={(e) => setProfessorConfirmPassword(e.target.value)}
                  startAdornment={
                    <InputAdornment position="start">
                      <img
                        src="/icons/password-icon.svg"
                        style={{ height: "15px", width: "15px" }}
                      />
                    </InputAdornment>
                  }
                />

                <InputLabel htmlFor="profilePicture">
                  Profilna fotografija
                </InputLabel>
                <OutlinedInput
                  id="profilePicture"
                  type="file"
                  onChange={handleProfessorImageChange}
                />

                <InputLabel htmlFor="profilePicture">
                  Pridruži se predmetu{" "}
                </InputLabel>
                <Autocomplete
                  disablePortal
                  id="combo-box-demo"
                  options={subjects}
                  getOptionLabel={(option) => option.title}
                  onChange={handleSubjectSelect}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      InputProps={{
                        ...params.InputProps,
                        startAdornment: (
                          <InputAdornment position="start">
                            <img
                              src="/icons/search-icon.svg"
                              style={{ height: "20px", width: "20px" }}
                            />
                          </InputAdornment>
                        ),
                      }}
                    />
                  )}
                />
                {professorSubjects.map((subject) => (
                  <div key={subject.url} className="link-no-style">
                    <div className="predmet">
                      <h2 className="predmet-text">{subject.title}</h2>
                      <p className="predmet-text">{subject.description}</p>
                    </div>
                  </div>
                ))}
              </div>

              <Button
                type="submit"
                variant="contained"
                style={{ marginRight: "1rem" }}
              >
                Registriraj se
              </Button>
              <Button
                type="button"
                variant="contained"
                onClick={() => {
                  setProfessorName("");
                  setProfessorSurname("");
                  setProfessorEmail("");
                  setProfessorPassword("");
                  setProfessorConfirmPassword("");
                  setProfessorProfilePicture(null);
                }}
              >
                Odbaci
              </Button>
            </form>
          </div>
        </div>
      )}
      <div className="login-wrapper">
        <div className="login-container" style={{ flexDirection: "row" }}>
          <Button
            variant="contained"
            onClick={() => setShowStudentForm(!showStudentForm)}
          >
            Registriraj se kao {showStudentForm ? "professor" : "student"}?
          </Button>
        </div>
      </div>
    </>
  );
}

export default RegisterPage;

using Ins.Server.Models;
using Ins.Server.Utils;
using Ins.Server.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Ins.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubjectsController : Controller
    {
        private readonly Ins.Data.AppDbContext context;
        private readonly IConfiguration configuration;

        public SubjectsController(Ins.Data.AppDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Register(CreateSubjectModel model)
        {
            if (ModelState.IsValid)
            {
                var subject = new Subject
                {
                    Title = model.Title,
                    Url = model.Url,
                    Description = model.Description
                };

                context.Subjects.Add(subject);
                await context.SaveChangesAsync();

                var response = new { success = true, subject };
                return Ok(response);
            }

            return BadRequest(ModelState);
        }

        [HttpGet("{url}")]
        public async Task<IActionResult> GetSubjectByUrl(string url)
        {
            var subject = await context.Subjects.FirstOrDefaultAsync(s => s.Url == url);

            if (subject == null) return NotFound(new { success = false, message = "Subject not found." });

            var response = new
            {
                success = true,
                subject = new { subject.Id, subject.Title, subject.Url, subject.Description }
            };

            return Ok(response);
        }


        [HttpGet]
        public async Task<IActionResult> GetAllSubjects()
        {
            var subjects = await context.Subjects
                .Select(subject => new
                {
                    subject.Id,
                    subject.Title,
                    subject.Url,
                    subject.Description

                })
                .ToListAsync();

            return Ok(new { success = true, subjects });
        }

    }
}

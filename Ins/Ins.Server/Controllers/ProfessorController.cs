using Ins.Server.Models;
using Ins.Server.Utils;
using Ins.Server.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProfessorsController : Controller
    {
        private readonly Ins.Data.AppDbContext context;
        private readonly IConfiguration configuration;

        public ProfessorsController(Ins.Data.AppDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }



        [HttpPost("register")]
        public async Task<IActionResult> Register(ProfessorRegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                var professor = new Professor
                {
                    Name = model.Name,
                    Surname = model.Surname,
                    Email = model.Email,
                    Password = PasswordUtils.HashPassword(model.Password),
                    ProfilePictureUrl = model.ProfilePictureUrl,
                    Subjects = model.Subjects
                };

                context.Professors.Add(professor);
                await context.SaveChangesAsync();

                var response = new { success = true, professor };
                return Ok(response);
            }

            return BadRequest(ModelState);
        }




        [HttpPost("login")]
        public async Task<IActionResult> Login(ProfessorLoginModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Professor? professor = null;

            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                if (RegexUtils.IsValidEmail(model.Email))
                    professor = await context.Professors.FirstOrDefaultAsync(u => u.Email == model.Email);
            }

            if (professor == null)
            {
                return Unauthorized(new { success = false, message = "Authentication failed. Student not found." });
            }

            var passwordIsValid = PasswordUtils.VerifyPassword(model.Password, professor.Password);
            if (!passwordIsValid)
            {
                return Unauthorized(new { success = false, message = "Authentication failed. Incorrect password." });
            }

            var token = GenerateJwtToken(professor);

            var response = new
            {
                success = true,
                message = "Login successful",
                professor = new
                {
                    professor.Id,
                    professor.Name,
                    professor.Surname,
                    professor.Email,
                    professor.ProfilePictureUrl,
                    professor.InstructionsCount,
                    professor.Subjects
                },
                token
            };

            return Ok(response);
        }

        private string GenerateJwtToken(Professor professor)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, professor.Surname),
                new Claim(JwtRegisteredClaimNames.Email, professor.Email),
                new Claim("id", professor.Id.ToString()),
            };

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddHours(24),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        [HttpGet("{email}")]
        public async Task<IActionResult> GetProfessorByEmail(string email)
        {
            var professor = await context.Professors.FirstOrDefaultAsync(s => s.Email == email);

            if (professor == null)
                return NotFound(new { success = false, message = "Professor not found." });

            var response = new
            {
                success = true,
                professor = new
                {
                    professor.Id,
                    professor.Name,
                    professor.Surname,
                    professor.Email,
                    professor.ProfilePictureUrl,
                    professor.Password,
                    professor.InstructionsCount,
                    professor.Subjects
                }
            };

            return Ok(response);
        }


        [HttpGet]
        public async Task<IActionResult> GetAllProfessors()
        {
            var professors = await context.Professors
                .Select(professor => new {
                    professor.Id,
                    professor.Name,
                    professor.Surname,
                    professor.Email,
                    professor.ProfilePictureUrl,
                    professor.Password,
                    professor.InstructionsCount,
                    professor.Subjects

                })
                .ToListAsync();

            return Ok(new { success = true, professors });
        }

    }
}

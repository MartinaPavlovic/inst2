using Ins.Server.Models;
using Ins.Server.Utils;
using Ins.Server.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Ins.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {
        private readonly Data.AppDbContext context;
        private readonly IConfiguration configuration;

        public StudentsController(Data.AppDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }


        [HttpPost("register")]
        public async Task<IActionResult> Register(StudentRegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                var student = new Student
                {
                    Name = model.Name,
                    Surname = model.Surname,
                    Email = model.Email,
                    Password = PasswordUtils.HashPassword(model.Password),
                    ProfilePictureUrl = model.ProfilePictureUrl
                };

                context.Students.Add(student);
                await context.SaveChangesAsync();

                var response = new { success = true, student };
                return Ok(response);
            }

            return BadRequest(ModelState);
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login(StudentLoginModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Student? student = null;

            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                if (RegexUtils.IsValidEmail(model.Email))
                    student = await context.Students.FirstOrDefaultAsync(u => u.Email == model.Email);
            }

            if (student == null)
            {
                return Unauthorized(new { success = false, message = "Authentication failed. Student not found." });
            }

            var passwordIsValid = PasswordUtils.VerifyPassword(model.Password, student.Password);
            if (!passwordIsValid)
            {
                return Unauthorized(new { success = false, message = "Authentication failed. Incorrect password." });
            }

            var token = GenerateJwtToken(student);

            var response = new
            {
                success = true,
                message = "Login successful",
                student = new { student.Id, student.Name, student.Surname, student.Email, student.ProfilePictureUrl },
                token
            };

            return Ok(response);
        }

        private string GenerateJwtToken(Student student)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, student.Surname),
                new Claim(JwtRegisteredClaimNames.Email, student.Email),
                new Claim("id", student.Id.ToString()),
            };

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddHours(24),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        [HttpGet("{email}")]
        public async Task<IActionResult> GetStudentByEmail(string email)
        {
            var student = await context.Students.FirstOrDefaultAsync(s => s.Email == email);

            if (student == null)
                return NotFound(new { success = false, message = "Student not found." });

            var response = new
            {
                success = true,
                student = new { student.Id, student.Name, student.Surname, student.Email, student.ProfilePictureUrl, student.Password }
            };

            return Ok(response);
        }


        [HttpGet]
        public async Task<IActionResult> GetAllStudents()
        {
            var students = await context.Students
                .Select(student => new {
                    student.Id,
                    student.Name,
                    student.Surname,
                    student.Email,
                    student.ProfilePictureUrl,
                    student.Password

                })
                .ToListAsync();

            return Ok(new { success = true, students });
        }

    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Ins.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InstrctionsDatesController : Controller
    {
        private readonly Ins.Data.AppDbContext context;
        private readonly IConfiguration configuration;

        public InstrctionsDatesController(Ins.Data.AppDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }



        [HttpGet("{id}")]
        public async Task<IActionResult> GetStudentByID(int id)
        {
            var instructionsDate = await context.InstructionsDates.FindAsync(id);

            if (instructionsDate == null) return NotFound(new { success = false, message = "InstrctionsDate not found." });

            var response = new
            {
                success = true,
                instructionsDate = new { instructionsDate.Id, instructionsDate.StudentId, instructionsDate.ProfessorId, instructionsDate.DateTime, instructionsDate.Status }
            };

            return Ok(response);
        }


        [HttpGet]
        public async Task<IActionResult> GetAllInstructionsDates()
        {
            var instructionsDates = await context.InstructionsDates
                .Select(instructionsDates => new
                {
                    instructionsDates.Id,
                    instructionsDates.StudentId,
                    instructionsDates.ProfessorId,
                    instructionsDates.DateTime,
                    instructionsDates.Status

                })
                .ToListAsync();

            return Ok(new { success = true, instructionsDates });
        }

    }
}

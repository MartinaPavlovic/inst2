namespace Ins.Server.ViewModels
{
    public class StudentLoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}

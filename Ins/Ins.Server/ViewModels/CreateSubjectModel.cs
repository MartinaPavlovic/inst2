namespace Ins.Server.ViewModels
{
    public class CreateSubjectModel
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
    }
}
namespace Ins.Server.Models
{
    public class InstructionsDate
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int ProfessorId { get; set; }
        public DateTime DateTime { get; set; }
        public string Status { get; set; }

    }
}
using Ins.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Ins.Server
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllers();
            SetUpDB(builder);
            SetUpJWT(builder);

            var app = builder.Build();

            TestDatabaseConnection(app.Services);

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication(); //has to go before UseAuthorization
            app.UseAuthorization();
            app.MapControllers();
            app.Run();
        }

        private static void SetUpDB(WebApplicationBuilder builder)
        {
            builder.Services.AddDbContext<Data.AppDbContext>(options =>
                            options.UseNpgsql(builder.Configuration.GetConnectionString("AppContextConnection")));
        }


        private static void SetUpJWT(WebApplicationBuilder builder)
        {
            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]))
                };
            });

            builder.Services.AddAuthorization();
        }

        private async static void TestDatabaseConnection(IServiceProvider services)
        {
            await Task.Delay(3000);

            using (var scope = services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<Data.AppDbContext>();
                try
                {
                    dbContext.Database.OpenConnection();
                    dbContext.Database.CloseConnection();
                    Console.WriteLine("Database connection successful.");
                    foreach (var student in dbContext.Students.ToList())
                    {
                        Console.WriteLine($"Name: {student.Name}, Username: {student.Surname}, ID: {student.Id}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Database connection failed: {ex.Message}");
                }
            }
        }
    }
}
